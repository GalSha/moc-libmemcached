#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <limits.h>
#include <string.h>

int readNum(FILE *f, unsigned int *num) {
    int i = 0, sum = 0;
    char *t = (char *) num;
    for (i = 0; i < sizeof(*num); i++)
        sum += fscanf(f, "%c", t + i);
    return sum;
}

//the number which is used as refernce to MAX_EXEC time: MAX_EXEC time = Exec_time_on(MAX_EXEC_NUM)*(1+noise_margin)
#define MAX_EXEC_NUM 4035952830U

#define MAX_EXEC 10300
#define MIN_EXEC -1
#define NUM_WEIGHT 256

static int cal_weight(long exec_time) {
    if (exec_time <= MIN_EXEC)
        return 256;
    if (exec_time >= MAX_EXEC)
        return NUM_WEIGHT - 1;
    double weight = ((double) NUM_WEIGHT) / MAX_EXEC;
    weight *= exec_time;
    return (int) (weight);
}

int compute_dividers(unsigned int num, unsigned int *res) {
    struct timespec s, e;
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &s);

    unsigned int it;
    unsigned int limit = (num / 2) + 1;
    int j = 0;

    memset((char *) res, 0, 512 * sizeof(unsigned int));
    res[j] = 1;
    j++;
    for (it = 2; it < limit; it++) {
        if (num % it == 0) {
            res[j] = it;
            j++;
        }
    }

    res[j] = num;

    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &e);

    long diff = e.tv_sec - s.tv_sec;
    diff *= 1000000000;
    diff += e.tv_nsec - s.tv_nsec;

    return cal_weight(diff / 1000000);
}


int main() {
    unsigned int res[512];
    FILE *r = fopen("/dev/urandom", "r");
    if (r == NULL) {
        printf("file open err");
        return -1;
    }
    struct timespec s, e;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &s);

    unsigned int num = 1;
    int check = readNum(r, &num);
    while (1) {
        if (check != 4) {
            printf("check error: %d %u\n", check, num);
            break;
        }
        int weight = compute_dividers(num, res);
        printf("%u %d\n", num, weight);

        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &e);
        if ((e.tv_sec - s.tv_sec) > 60 * 60 * 12)
            break;
        check = readNum(r, &num);
    }
    fclose(r);
    return 0;
}
