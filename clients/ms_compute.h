//
// Created by Gal on 11/11/2018.
//
//* Use and distribution licensed under the BSD license.  See
//* the COPYING file in the parent directory for full text.
//

#ifndef MOC_LIBMEMCACHED_MS_COMPUTE_H
#define MOC_LIBMEMCACHED_MS_COMPUTE_H

#include <stdint-gcc.h>

int cal_weight(long exec_time);
int compute_dividers(uint32_t num, uint32_t* res);

#endif //MOC_LIBMEMCACHED_MS_COMPUTE_H
