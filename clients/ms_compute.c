//
// Created by Gal on 11/11/2018.
//
//* Use and distribution licensed under the BSD license.  See
//* the COPYING file in the parent directory for full text.
//


#include "mem_config.h"

//#include <unistd.h>
#include <sys/types.h>
#include <string.h>

#if defined(HAVE_SYS_TIME_H)
# include <sys/time.h>
#endif

#if defined(HAVE_TIME_H)
# include <time.h>
#endif

#include "ms_compute.h"



#define MAX_EXEC_NUM 4035952830U
#define MAX_EXEC 10300
#define MIN_EXEC -1
#define NUM_WEIGHT 256

int cal_weight(long exec_time)
{
    if(exec_time<=MIN_EXEC)
        return 256;
    if(exec_time >= MAX_EXEC)
        return NUM_WEIGHT-1;
    double weight = ((double)NUM_WEIGHT) / MAX_EXEC;
    weight *= exec_time;
    return (int)(weight);
}

int compute_dividers(uint32_t num, uint32_t* res)
{
    //num = MAX_EXEC_NUM;
    //struct timespec start, end;

    struct timespec s,e;
    clock_gettime(CLOCK_THREAD_CPUTIME_ID,&s);

    //clock_gettime(CLOCK_PROCESS_CPUTIME_ID,&start);


    unsigned int it;
    unsigned int limit = (num/2) +1;
    int j =0;

    memset(res,0,512*sizeof(uint32_t));
    res[j] = 1;
    j++;
    for (it = 2; it < limit; it++) {
        if(num%it==0)
        {
            res[j]=it;
            j++;
        }
    }

    res[j]=num;

    clock_gettime(CLOCK_THREAD_CPUTIME_ID,&e);

    long  diff = e.tv_sec- s.tv_sec;
    diff *= 1000000000;
    diff += e.tv_nsec - s.tv_nsec;

    /*
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID,&end);

    long  diffu = end.tv_sec- start.tv_sec;
    diffu *= 1000000000;
    diffu += end.tv_nsec - start.tv_nsec;

    printf("*******\nStarted at: %ld.%09lds\n", start.tv_sec, start.tv_nsec);
    printf("Ended at: %ld.%09lds\n", end.tv_sec, end.tv_nsec);
    printf("Diff :%ld \n\n",diffu);


     printf("Started at: %ld.%09lds\n", s.tv_sec, s.tv_nsec);
     printf("Ended at: %ld.%09lds\n", e.tv_sec, e.tv_nsec);
    printf("%u : %ld : %d\n**********\n\n",num,diff/1000000, weight(diff/1000000));
*/
    return cal_weight(diff/1000000);
}
